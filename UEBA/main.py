import sys
import gc

import pandas as pd
from tqdm import tqdm
from sklearn.preprocessing import StandardScaler, MinMaxScaler, LabelEncoder
from datetime import datetime
import numpy as np
from sklearn.ensemble import IsolationForest


def train(X_train):
    import numpy as np
    import random as rn
    import os
    import warnings

    def c_factor(n):
        return 2.0 * (np.log(n - 1) + 0.5772156649) - (2.0 * (n - 1.) / (n * 1.0))

    class iForest(object):
        def __init__(self, X, ntrees, sample_size, limit=None, ExtensionLevel=0):

            self.ntrees = ntrees
            self.X = X
            self.nobjs = len(X)
            self.sample = sample_size
            self.Trees = []
            self.limit = limit
            self.exlevel = ExtensionLevel
            self.CheckExtensionLevel()  # Extension Level check. See def for explanation.
            if limit is None:
                self.limit = int(np.ceil(np.log2(
                    self.sample)))  # Set limit to the default as specified by the paper (average depth of unsuccesful search through a binary tree).
            self.c = c_factor(self.sample)
            for i in range(self.ntrees):  # This loop builds an ensemble of iTrees (the forest).
                ix = rn.sample(range(self.nobjs), self.sample)
                X_p = X[ix]
                self.Trees.append(iTree(X_p, 0, self.limit, exlevel=self.exlevel))

        def CheckExtensionLevel(self):
            dim = self.X.shape[1]
            if self.exlevel < 0:
                raise Exception("Extension level has to be an integer between 0 and " + str(dim - 1) + ".")
            if self.exlevel > dim - 1:
                raise Exception(
                    "Your data has " + str(dim) + " dimensions. Extension level can't be higher than " + str(
                        dim - 1) + ".")

        def compute_paths(self, X_in=None):
            if X_in is None:
                X_in = self.X
            S = np.zeros(len(X_in))
            for i in range(len(X_in)):
                h_temp = 0
                for j in range(self.ntrees):
                    h_temp += PathFactor(X_in[i], self.Trees[j]).path * 1.0  # Compute path length for each point
                Eh = h_temp / self.ntrees  # Average of path length travelled by the point in all trees.
                S[i] = 2.0 ** (-Eh / self.c)  # Anomaly Score
            return S

    def postprocessing(df, scores_pred):
        df['ret'] = (scores_pred - min(scores_pred)) / (max(scores_pred) - min(scores_pred))
        df['ret'] = df.apply(lambda x: x.ret + 0.1 if x.account_port_nunique == 2 else x.ret, axis=1)
        df['ret'] = df['ret'] - np.mean(df['ret']) + 0.62
        df['ret'] = np.clip(df['ret'].to_numpy(), 0, 1)
        return df

    class Node(object):
        def __init__(self, X, n, p, e, left, right, node_type=''):
            self.e = e
            self.size = len(X)
            self.X = X  # to be removed
            self.n = n
            self.p = p
            self.left = left
            self.right = right
            self.ntype = node_type

    class iTree(object):
        def __init__(self, X, e, l, exlevel=0):
            self.exlevel = exlevel
            self.e = e
            self.X = X  # save data for now. Not really necessary.
            self.size = len(X)
            self.dim = self.X.shape[1]
            self.Q = np.arange(np.shape(X)[1], dtype='int')  # n dimensions
            self.l = l
            self.p = None  # Intercept for the hyperplane for splitting data at a given node.
            self.n = None  # Normal vector for the hyperplane for splitting data at a given node.
            self.exnodes = 0
            self.root = self.make_tree(X, e, l)  # At each node create a new tree, starting with root node.

        def make_tree(self, X, e, l):
            self.e = e
            if e >= l or len(X) <= 1:  # A point is isolated in traning data, or the depth limit has been reached.
                left = None
                right = None
                self.exnodes += 1
                return Node(X, self.n, self.p, e, left, right, node_type='exNode')
            else:  # Building the tree continues. All these nodes are internal.
                mins = X.min(axis=0)
                maxs = X.max(axis=0)
                idxs = np.random.choice(range(self.dim), self.dim - self.exlevel - 1,
                                        replace=False)  # Pick the indices for which the normal vector elements should be set to zero acccording to the extension level.
                self.n = np.random.normal(0, 1,
                                          self.dim)  # A random normal vector picked form a uniform n-sphere. Note that in order to pick uniformly from n-sphere, we need to pick a random normal for each component of this vector.
                self.n[idxs] = 0
                self.p = np.random.uniform(mins,
                                           maxs)  # Picking a random intercept point for the hyperplane splitting data.
                w = (X - self.p).dot(
                    self.n) < 0  # Criteria that determines if a data point should go to the left or right child node.
                return Node(X, self.n, self.p, e, \
                            left=self.make_tree(X[w], e + 1, l), \
                            right=self.make_tree(X[~w], e + 1, l), \
                            node_type='inNode')

    class PathFactor(object):
        def __init__(self, x, itree):
            self.path_list = []
            self.x = x
            self.e = 0
            self.path = self.find_path(itree.root)

        def find_path(self, T):
            if T.ntype == 'exNode':
                if T.size <= 1:
                    return self.e
                else:
                    self.e = self.e + c_factor(T.size)
                    return self.e
            else:
                p = T.p  # Intercept for the hyperplane for splitting data at a given node.
                n = T.n  # Normal vector for the hyperplane for splitting data at a given node.

                self.e += 1

                if (self.x - p).dot(n) < 0:
                    self.path_list.append('L')
                    return self.find_path(T.left)
                else:
                    self.path_list.append('R')
                    return self.find_path(T.right)

    def all_branches(node, current=[], branches=None):
        current = current[:node.e]
        if branches is None: branches = []
        if node.ntype == 'inNode':
            current.append('L')
            all_branches(node.left, current=current, branches=branches)
            current = current[:-1]
            current.append('R')
            all_branches(node.right, current=current, branches=branches)
        else:
            branches.append(current)
        return branches

    train_data = X_train[['hour']].to_numpy(dtype=np.double) # 转化为double类型
    sample_size = X_train.shape[0] # 取全量样本
    F0 = iForest(train_data, ntrees=1, sample_size=sample_size, ExtensionLevel=0) # 构筑孤立森林
    scores_pred = F0.compute_paths(X_in=X_train[['hour']].to_numpy(dtype=np.double)) # 预测分数
    X_train = postprocessing(X_train, scores_pred) # 进行归一化，放缩到0到1之间
    return X_train


def run(test_file, submission):
    def proprecessing(data_all):
        # 时间特征处理
        data_all['day'] = pd.to_datetime(data_all['time']).dt.day
        data_all['hour'] = pd.to_datetime(data_all['time']).dt.hour
        data_all['minute'] = pd.to_datetime(data_all['time']).dt.minute
        # 特征拼接
        join_cat_feat = []
        for f in tqdm([
            ['account', 'IP'],
            ['account', 'url'],
            ['account', 'switchIP'],
            ['account', 'url', 'IP'],
            ['url', 'IP', 'switchIP']
        ]):
            join_cat_feat.append('_'.join(f))
            data_all['_'.join(f)] = data_all[f[0]]
            for i in f:
                if i not in ['account']:
                    if i in ['port', 'vlan', 'hour']:
                        data_all['_'.join(f)] = data_all['_'.join(f)] + data_all[i].astype('str')
                    else:
                        data_all['_'.join(f)] = data_all['_'.join(f)] + data_all[i]

        # label encoding
        # 更好的label encoder应该每一位都分开
        cat_features = ['account', 'IP', 'url', 'switchIP', 'group'] + join_cat_feat
        le = LabelEncoder()
        for col in cat_features:
            i = data_all.columns.get_loc(col)
            data_all.iloc[:, i] = data_all.apply(lambda i: le.fit_transform(data_all[col]), axis=0,
                                                 result_type='expand')

        # 拼接特征的count
        for f in tqdm(join_cat_feat):
            data_all['{}__count'.format(f)] = data_all.groupby(f)[f].transform('count')

        # account
        agg_dic = {
            "IP": ['count', 'nunique'],
            "url": ['count', 'nunique'],
            "port": ['count', 'nunique'],
            "vlan": ['count'],
            "switchIP": ['count', 'nunique'],
            "hour": ['count', 'nunique', 'mean']
        }
        account_features = data_all.groupby('account').agg(agg_dic).reset_index()
        fea_names = ['_'.join(c) for c in account_features.columns]
        account_features.columns = ['account_' + c for c in fea_names]
        account_features.rename(columns={'account_account_': 'account'}, inplace=True)
        # 特征拼接
        data_all = data_all.merge(account_features, on='account', how='left')

        # hour特征
        data_all['mean_hour'] = data_all.groupby('account')['hour'].transform('mean')
        data_all['hour_sub'] = data_all['hour'] - data_all['mean_hour']

        dense_features = [f for f in data_all.columns if f not in ['id', 'group', 'time', 'ret'] + cat_features]

        data_all = data_all[cat_features + dense_features + ['id']]
        return data_all, cat_features, dense_features

    origin_test_data = pd.read_csv(test_file)
#     print(origin_test_data['group'])
    test_data, cat_features, dense_features = proprecessing(origin_test_data)
    # 执行孤立森林算法
    test_data = train(test_data)

    # origin_test_data['ret'] = norm_pred
    df = test_data[['id', 'ret']]
    df.to_csv(submission, index=None)
    print('to csv over')


if __name__ == '__main__':
    args = sys.argv[1:]
    if len(args) != 2:
        print('argument has error,' + str(len(args)) + ' not equal 2')
        print(args)
    else:
        run(args[0], args[1])