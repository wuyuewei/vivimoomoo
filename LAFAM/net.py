from keras.utils.vis_utils import plot_model
import pandas as pd
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers, Sequential, datasets
from tensorflow.keras.optimizers import Adam
from keras.layers import Dense, Dropout, Add
from keras import Model
from keras import Input
import keras.backend as K
from keras.callbacks import TensorBoard
import numpy as np

# 输入训练集
Xin = pd.read_csv('data_standard.csv',
                  usecols=(2, 3, 5, 7, 8, 9, 13, 14, 16, 17, 18, 19, 20), delimiter=",", encoding='gbk', skiprows=1)
Yin = pd.read_csv('data_standard.csv', usecols=(15,), delimiter=",",
                  encoding='gbk', skiprows=1)
Xin = Xin.values.reshape(Xin.shape)
Yin = Yin.values.reshape(Yin.shape[0], 1)
Xin = Xin.astype('float64')
Yin = Yin.astype('float64')
train_data = Xin[10000:-1, :]
train_label = Yin[10000:-1, :]
test_data = Xin[:10000, :]
test_label = Yin[:10000, :]
train_data2 = train_data[:, (1, 2, 3, 4, 8, 5, 7, 12)]
train_data3 = train_data[:, (0, 1, 5, 6, 7, 9, 10, 11, 12)]
test_data2 = test_data[:, (1, 2, 3, 4, 8, 5, 7, 12)]
test_data3 = test_data[:, (0, 1, 5, 6, 7, 9, 10, 11, 12)]

n_inputs1 = 13
n_hidden11 = 32
n_hidden12 = 32
n_outputs1 = 1
n_inputs2 = 8
n_hidden21 = 32
n_hidden22 = 32
n_outputs2 = 1
n_inputs3 = 9
n_hidden31 = 32
n_hidden32 = 32
n_outputs3 = 1
n_inputs4 = 13
n_hidden41 = 32
n_hidden42 = 32
n_outputs4 = 3


# assume two only - bad practice, but for illustration...
def new_multiply(inputs):
    return tf.multiply(inputs[0], inputs[1])


def focal_loss(y_true, y_pred):
  gamma = 2
  alpha = 0.58
  pt_1 = tf.where(y_true >= 0.15, y_pred, tf.ones_like(y_pred))
  pt_0 = tf.where(y_true < 0.15, y_pred, tf.zeros_like(y_pred))
  ls = -K.sum(alpha * K.pow(1. - pt_1, gamma) * K.log(pt_1))-K.sum((1.-alpha) * K.pow(pt_0, gamma) * K.log(1. - pt_0))
  return ls


def shrinkage_loss(y_true, y_pred):
    a = 17
    c = 0.1
    l = K.abs(y_pred - y_true)
    l2 = l ** 2
    w1 = a * (c - l)
    ls = l2 / (1 + K.exp(w1))
    return ls

def new_loss(y_true, y_pred):
  l = K.abs(y_pred - y_true)
  alpha = 52
  beta = 0.08
  gamma = 2.12
  l2 = l**gamma
  ls = 0.5*(1+alpha*(l-beta)/(1+alpha*K.abs(l-beta)))*l2
  #ls = 0.5*(0.3+0.4*y_true)*(1+alpha*(l-beta)/(1+alpha*K.abs(l-beta)))*l2
  return ls

# 输入层
inputA = Input(shape=(n_inputs1,), name="input1")
inputB = Input(shape=(n_inputs2,), name="input2")
inputC = Input(shape=(n_inputs3,), name="input3")
inputD = Input(shape=(n_inputs1,), name="input4")
label_layer = Input((n_outputs1))

# 全特征分类器部分
x = Dense(8, activation="relu",name="Dense11")(inputA)
x = Dense(8, activation="relu", name="Dense12")(x)
x = Dense(1, activation="sigmoid", name="Dense13")(x)
# x = Model(inputs=inputA, outputs=x)

# 第一个部分特征分类器
y = Dense(8, activation="relu", name="Dense21")(inputB)
y = Dense(8, activation="relu", name="Dense22")(y)
y = Dense(1, activation="sigmoid", name="Dense23")(y)
# y = Model(inputs=inputB, outputs=y)

# 第二个部分特征分类器
z = Dense(8, activation="relu", name="Dense31")(inputC)
z = Dense(8, activation="relu", name="Dense32")(z)
z = Dense(1, activation="sigmoid", name="Dense33")(z)
# z = Model(inputs=inputC, outputs=z)

# loss预测分类器
w = Dense(8, activation="relu", name="Dense41")(inputD)
w = Dense(8, activation="relu", name="Dense42")(w)
w = Dense(3, name="Dense43")(w)
# w = Model(inputs=inputD, outputs=w)

# 加权输出y_pred过程
# combined = K.concatenate([x.output, y.output, z.output])
combined = K.concatenate([x, y, z])
w = tf.nn.softmax(-1*w,axis=1)
# combined = tf.nn.softmax(-1*combined, axis=1)
# o = new_multiply([combined, w.output])
o = new_multiply([combined, w])
s = tf.reduce_sum(o, axis=1, keepdims=True)


model = Model(inputs=[inputA, inputB, inputC, inputD, label_layer], outputs=[s,x,y,z])


loss1 = focal_loss(label_layer, x)
loss2 = focal_loss(label_layer, y)
loss3 = focal_loss(label_layer, z)
loss4 = focal_loss(label_layer, s)
loss_w = tf.concat([loss1, loss2, loss3], -1)
loss_l = tf.losses.mean_squared_error(loss_w, w)
loss = tf.reduce_mean(
    (loss4 + loss_l+tf.reduce_sum(tf.multiply(K.softmax(w, axis=1), loss_w), 1)), name="loss")

    # model.add_loss(loss1)
    # model.add_loss(loss2)
    # model.add_loss(loss3)
model.add_loss(loss)

model.summary()


model.compile(optimizer=Adam(lr=0.01), loss=[
              None, focal_loss, focal_loss, focal_loss])


model.fit([train_data, train_data2, train_data3, train_data, train_label], train_label, batch_size=64, epochs=20,
          shuffle=True, validation_split=0.2)
mp = "model1.h5"
model.save(mp)


y_pred2 = model.predict(
    [train_data, train_data2, train_data3, train_data, train_label])
y_pred3 = model.predict(
    [test_data, test_data2, test_data3, test_data, test_label])
np.savetxt(
    'focal3.txt', y_pred2[0])
np.savetxt(
    'focal4.txt', y_pred3[0])
