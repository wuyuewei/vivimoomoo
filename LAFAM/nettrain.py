from keras.optimizers import Adam, SGD
from sklearn.metrics import precision_score, f1_score
from sklearn.metrics import recall_score, accuracy_score
from keras.models import load_model
from sklearn.model_selection import train_test_split
from sklearn.metrics import auc
from sklearn.metrics import roc_curve
import keras
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import tensorflow as tf
import math
from keras import backend as K
from keras.layers import Dense, Dropout
from keras.models import Sequential
from keras.optimizers import Adam
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import confusion_matrix
from keras.callbacks import TensorBoard


model = load_model('model1.h5')
model.summary()
Xin = pd.read_csv('data_standard.csv',
                  usecols=(2, 3, 5, 7, 8, 9, 13, 14, 16, 17, 18, 19, 20), delimiter=",", encoding='gbk', skiprows=1)
Yin = pd.read_csv('data_standard.csv', usecols=(15,), delimiter=",",
                  encoding='gbk', skiprows=1)
Xin = Xin.values.reshape(Xin.shape)
Yin = Yin.values.reshape(Yin.shape[0], 1)
Xin = Xin.astype('float64')
Yin = Yin.astype('float64')
train_data = Xin[10000:-1, :]
train_label = Yin[10000:-1, :]
train_data2 = train_data[:, (1, 2, 3, 4, 8, 5, 7, 12)]
train_data3 = train_data[:, (0, 1, 5, 6, 7, 9, 10, 11, 12)]

model.compile(optimizer=Adam(lr=0.01), metrics=[
              'accuracy'])
model.fit([train_data, train_data2, train_data3, train_data, train_label], train_label, batch_size=64, epochs=50,
          shuffle=True, validation_split=0.2)

mp = "model1.h5"
model.save(mp)
