import pickle
import random

import numpy as np
from sklearn.utils import shuffle

import keras
from keras.layers import Dense, Dropout, Activation
from keras.preprocessing.text import Tokenizer
from sklearn.model_selection import train_test_split
from keras.preprocessing import sequence
from keras.layers import Dense, Dropout, Activation
from keras.layers import Embedding
from keras.layers import LSTM, Bidirectional
from keras.preprocessing.sequence import pad_sequences
from keras.layers import Input, Flatten
from keras.models import Model
from sklearn import metrics
from keras import backend as K


f = open('./dataset/train.pkl','rb')
train = pickle.load(f)
phone = set()
for i in train.keys():
    phone.add(i[:-2])
#print(phone)
phone = list(phone)
#print("###########################")
print(len(phone))
X = []
Y = []
import numpy as np

for key in train.keys():
    tmp = np.asarray(train[key]) * 1e4

    train[key] = tmp.tolist()
for i in phone:
    a = train[i + '-A']
    b = train[i + '-B']
    X.append([a, b])
    Y.append(1)

for i in phone:
    for j in phone:
        if i != j :
            a = train[i+'-A']
            b = train[j+'-B']
            X.append([a,b])
            Y.append(0)

for i in phone:
    for j in phone:
        if i != j :
            a = train[i+'-A']
            b = train[j+'-A']
            X.append([a,b])
            Y.append(0)

for i in phone:
    for j in phone:
        if i != j :
            a = train[i+'-B']
            b = train[j+'-B']
            X.append([a,b])
            Y.append(0)

X = X[:9600]
Y = Y[:9600]
len(Y)

X,Y = shuffle(X,Y)

EMBEDDING_DIM = 100
MAX_A_LENGTH = 12
MAX_B_LENGTH = 12


def my_loss(y_true, y_pred):  # 1为小类，0为大类

    return (1.0 - K.cast(K.argmax(y_true), 'float32')) * K.categorical_crossentropy(y_pred, y_true) + (
                K.cast(K.argmax(y_true), 'float32') - 0.0) * K.categorical_crossentropy(y_pred, y_true) * 20

# 输入
A_sequence_input = Input(shape=(MAX_A_LENGTH,EMBEDDING_DIM), dtype='float32')
B_sequence_input = Input(shape=(MAX_B_LENGTH,EMBEDDING_DIM), dtype='float32')

# 双向LSTM
A_lstm = Bidirectional(LSTM(12, return_sequences = False))

encoded_A = A_lstm(A_sequence_input)
encoded_A_dropout = Dropout(.2)(encoded_A)

encoded_B = A_lstm(B_sequence_input)
encoded_B_dropout = Dropout(.2)(encoded_B)

merged_vector = keras.layers.concatenate([encoded_A_dropout, encoded_B_dropout], axis = -1)
merged_vector = Dense(24)(merged_vector)
merged_vector = Dropout(.2)(merged_vector)

preds = Dense(2, activation='softmax')(merged_vector)

model = Model(inputs=[A_sequence_input, B_sequence_input], outputs=preds)

model.compile(optimizer='adam',
              loss=my_loss,
              metrics=['accuracy'])

model.summary()

Y = keras.utils.to_categorical(np.asarray(Y),2)

A = []
B = []

for i in X:
    A.append(i[0])
    B.append(i[1])

A = pad_sequences(A, maxlen=MAX_A_LENGTH)
B = pad_sequences(B, maxlen=MAX_B_LENGTH)

print(A.shape,B.shape)

model.fit([A, B], Y,batch_size=2048,epochs=500,validation_split=0.1,verbose=1)

f = open('./dataset/test.pkl','rb')
test = pickle.load(f)

phone = set()
for i in test.keys():
    phone.add(i[:-2])
#print(phone)
phone = list(phone)
#print("###########################")
print(len(phone))

X = []
Y = []

import numpy as np

for key in test.keys():
    tmp = np.asarray(test[key]) * 1e4

    test[key] = tmp.tolist()

for i in phone:
    a = test[i+'-A']
    b = test[i+'-B']
    X.append([a,b])
    Y.append(1)

for i in phone:
    for j in phone:
        if i != j :
            a = test[i+'-A']
            b = test[j+'-B']
            X.append([a,b])
            Y.append(0)

for i in phone:
    for j in phone:
        if i != j :
            a = test[i+'-A']
            b = test[j+'-A']
            X.append([a,b])
            Y.append(0)

for i in phone:
    for j in phone:
        if i != j :
            a = test[i+'-B']
            b = test[j+'-B']
            X.append([a,b])
            Y.append(0)

X = X[:1200]
Y = Y[:1200]

import numpy as np
from sklearn.utils import shuffle

X,Y = shuffle(X,Y)

A = []
B = []

for i in X:
    A.append(i[0])
    B.append(i[1])

A = pad_sequences(A, maxlen=MAX_A_LENGTH)
B = pad_sequences(B, maxlen=MAX_B_LENGTH)

print(A.shape,B.shape)

pred = model.predict([A,B])

y_pred = np.argmax(pred,axis=-1)
y_true = Y

from sklearn.metrics import confusion_matrix
mat = confusion_matrix(y_true,y_pred)
print(mat)

from sklearn.metrics import classification_report
print(classification_report(y_true,y_pred))

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from sklearn.metrics import confusion_matrix
import numpy as np

labels = [0, 1]
cm = confusion_matrix(y_true, y_pred, labels=labels)
plt.matshow(cm)
plt.colorbar()
plt.ylabel('True label')
plt.xlabel('Predicted label')
plt.xticks(np.arange(cm.shape[1]), labels)
plt.yticks(np.arange(cm.shape[1]), labels)
plt.show()