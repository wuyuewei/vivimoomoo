
# coding: utf-8

# # 训练

# ## 导入训练数据

# In[1]:


import pickle


# In[2]:


f = open('./dataset/train.pkl','rb')
train = pickle.load(f)


# In[3]:


phone = set()
for i in train.keys():
    phone.add(i[:-2])
#print(phone)
phone = list(phone)
#print("###########################")
print(len(phone))


# In[4]:


X = []
Y = []


# ## 扩大向量的值

# In[5]:


import numpy as np
for key in train.keys():
    
    tmp = np.asarray(train[key]) * 10
    
    train[key] = tmp.tolist() 


# ## 获取label为1的训练数据

# In[6]:


for i in phone:
    a = train[i+'-A']
    b = train[i+'-B']
    X.append([a,b])
    Y.append(1)


# In[7]:


len(X)


# ## 获取label为0的训练数据
# 
# + 不平衡比例为5：1，此类数据的数量为8000组

# In[8]:


import random


# In[9]:


random.shuffle(phone)
for i in phone[:200]:
    random.shuffle(phone) 
    index = 0
    for j in phone:
        if i != j :
            a = train[i+'-A']
            b = train[j+'-B']
            X.append([a,b])
            Y.append(0)
            index += 1
        
        if index == 40:         
            break


# In[10]:


len(Y)


# ## 训练数据打乱

# In[11]:


import numpy as np
from sklearn.utils import shuffle


# In[12]:


X,Y = shuffle(X,Y)


# ## 构建Siamese模型

# In[13]:


import keras
from keras.layers import Dense, Dropout, Activation
from keras.preprocessing.text import Tokenizer
from sklearn.model_selection import train_test_split
from keras.preprocessing import sequence
from keras.layers import Dense, Dropout, Activation
from keras.layers import Embedding
from keras.layers import LSTM, Bidirectional
from keras.preprocessing.sequence import pad_sequences
from keras.layers import Input, Flatten
from keras.models import Model
from sklearn import metrics
from keras import backend as K
from keras.layers import Embedding,Lambda
from keras.layers.normalization import BatchNormalization
from keras.optimizers import SGD,RMSprop,Adam
from keras.callbacks import TensorBoard


# In[14]:


EMBEDDING_DIM = 100
MAX_A_LENGTH = 12
MAX_B_LENGTH = 12


# ## 定义的损失函数

# In[15]:


def contrastive_loss(y_true, y_pred):
    
    l2 = 1.8

    return (
             2 * ( y_true - 0.0 ) * K.mean(K.square(K.maximum(0.0, l2 - y_pred))) +
             ( 1.0 - y_true ) * K.mean(K.square(y_pred))
    )

def mean_absolute_error(y_true, y_pred):
    return K.mean(K.abs(y_pred - y_true), axis=-1)

def binary_crossentropy(y_true, y_pred):
    return K.mean(K.binary_crossentropy(y_pred, y_true), axis=-1)


# ## 定义的距离函数

# In[16]:


import keras.backend as K
def exp_distance(vects):
    x, y = vects
    return K.exp( 2.5 - K.sqrt(K.sum(K.square(x - y), axis=1, keepdims=True)))

def dist_output_shape(shapes):
    shape1, shape2 = shapes
    return (shape1[0], 1)


# In[17]:


# 输入
A_sequence_input = Input(shape=(MAX_A_LENGTH,EMBEDDING_DIM), dtype='float32')
B_sequence_input = Input(shape=(MAX_B_LENGTH,EMBEDDING_DIM), dtype='float32')

# 双向LSTM
shared_lstm = Bidirectional(LSTM(12, return_sequences = False))

BN_A = BatchNormalization()(A_sequence_input)
encoded_A = shared_lstm(BN_A)

BN_B = BatchNormalization()(B_sequence_input)
encoded_B = shared_lstm(BN_B)

encoded_A = Dropout(rate=0.1)(encoded_A)
encoded_A = Dense(units=128)(encoded_A)


encoded_B = Dropout(rate=0.1)(encoded_B)
encoded_B = Dense(units=128)(encoded_B)

distance = Lambda(exp_distance,output_shape=dist_output_shape)([encoded_A, encoded_B])

model = Model(inputs=[A_sequence_input, B_sequence_input], outputs=distance)    

# optimizer
rms = RMSprop()
adam = Adam()

model.compile(optimizer=adam,
              loss=binary_crossentropy,  # 'categorical_crossentropy',
              metrics=['accuracy'])


# In[18]:


## 打印Model的结构
model.summary()


# ## 开始训练

# In[19]:


#Y = keras.utils.to_categorical(np.asarray(Y),2)


# In[20]:


A = []
B = []

for i in X:
    A.append(i[0])
    B.append(i[1])

    
A_pad = []
B_pad = []

num_per_unit = 10000
for i in range(0,len(X),num_per_unit):
    A1 = pad_sequences(A[i:i+num_per_unit], maxlen=MAX_A_LENGTH)
    B1 = pad_sequences(B[i:i+num_per_unit], maxlen=MAX_B_LENGTH) 
    
    A_pad += A1.tolist()
    B_pad += B1.tolist()

A = np.asarray(A_pad)
B = np.asarray(B_pad)


# In[21]:


print(A.shape,B.shape)


# In[22]:


model.fit([A, B], Y,batch_size=2048,epochs=500,validation_split=0.1,verbose=1,callbacks=[TensorBoard(log_dir='./log/')])


# # 进行测试

# In[ ]:


f = open('./dataset/test.pkl','rb')
test = pickle.load(f)


# In[ ]:


phone = set()
for i in test.keys():
    phone.add(i[:-2])
#print(phone)
phone = list(phone)
#print("###########################")
print(len(phone))


# In[ ]:


X = []
Y = []


# ## 扩大向量的值

# In[ ]:


import numpy as np
for key in test.keys():
    
    tmp = np.asarray(test[key]) * 10
    
    test[key] = tmp.tolist() 


# ## 获取label为1的训练数据

# In[ ]:


for i in phone:
    a = test[i+'-A']
    b = test[i+'-B']
    X.append([a,b])
    Y.append(1)


# In[ ]:


len(X)


# ## 获取label为0的训练数据
# 
# + 不平衡比例为5：1，此类数据的数量为1000组

# In[ ]:


random.shuffle(phone)
for i in phone[:40]:
    random.shuffle(phone) 
    index = 0
    for j in phone:
        if i != j :
            a = test[i+'-A']
            b = test[j+'-B']
            X.append([a,b])
            Y.append(0)
            index += 1
        
        if index == 25:         
            break
            
           


# In[ ]:


len(Y)


# ## 训练数据打乱

# In[ ]:


import numpy as np
from sklearn.utils import shuffle


# In[ ]:


X,Y = shuffle(X,Y)


# ## 预测

# In[ ]:


A = []
B = []

for i in X:
    A.append(i[0])
    B.append(i[1])

A = pad_sequences(A, maxlen=MAX_A_LENGTH) 
B = pad_sequences(B, maxlen=MAX_B_LENGTH) 


# In[ ]:


print(A.shape,B.shape)


# In[ ]:


pred = model.predict([A,B])


# ## 由于本实验是关于用户关联，所以找出关联的用户比找出不关联的用户有价值的多，所以会在牺牲总体指标的情况下去提升类1的precision，做到宁缺毋滥

# In[ ]:


y_pred = []

threshold = 0.5

for dist in pred:
    if dist[0] > threshold:
        y_pred.append(1)
    else:
        y_pred.append(0)

y_true = Y


# In[ ]:


from sklearn.metrics import confusion_matrix
mat = confusion_matrix(y_true,y_pred)
print(mat)


# In[ ]:


from sklearn.metrics import classification_report
print(classification_report(y_true,y_pred))


# In[ ]:


import matplotlib  
import matplotlib.pyplot as plt  
import matplotlib.cm as cm 
from sklearn.metrics import confusion_matrix 
import numpy as np

 
labels = [0,1] 
cm = confusion_matrix(y_true, y_pred,labels=labels)  
plt.matshow(cm)  
plt.colorbar()
plt.ylabel('True label')  
plt.xlabel('Predicted label')  
plt.xticks(np.arange(cm.shape[1]),labels)  
plt.yticks(np.arange(cm.shape[1]),labels)  
plt.show() 

